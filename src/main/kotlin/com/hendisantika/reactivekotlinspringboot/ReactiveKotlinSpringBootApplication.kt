package com.hendisantika.reactivekotlinspringboot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReactiveKotlinSpringBootApplication

fun main(args: Array<String>) {
    runApplication<ReactiveKotlinSpringBootApplication>(*args)
}
