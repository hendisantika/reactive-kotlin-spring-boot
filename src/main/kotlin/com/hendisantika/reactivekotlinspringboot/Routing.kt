package com.hendisantika.reactivekotlinspringboot

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-kotlin-spring-boot
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/08/18
 * Time: 07.09
 * To change this template use File | Settings | File Templates.
 */
@Configuration
class Routing {

    @Bean
    fun booksRouter(handler: BooksHandler) = router {
        ("/books" and accept(MediaType.APPLICATION_JSON)).nest {
            GET("/", handler::getAll)
            GET("/{title}", handler::getBook)
            POST("/", handler::addBook)
        }
    }

}