package com.hendisantika.reactivekotlinspringboot

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.runApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-kotlin-spring-boot
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/08/18
 * Time: 07.19
 * To change this template use File | Settings | File Templates.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ExtendWith(SpringExtension::class)
class TestBase {

    protected val client = WebTestClient.bindToServer()
            .baseUrl("http://127.0.0.1:8080")
            .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .build()

    @BeforeAll
    fun initApplication() {
        runApplication<ReactiveKotlinSpringBootApplication>()
    }

}