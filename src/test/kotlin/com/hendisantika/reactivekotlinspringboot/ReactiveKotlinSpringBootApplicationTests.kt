package com.hendisantika.reactivekotlinspringboot

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient

@RunWith(SpringRunner::class)
@SpringBootTest
class ReactiveKotlinSpringBootApplicationTests {

    @Autowired
    private lateinit var repository: BookRepository

    protected val client = WebTestClient.bindToServer()
            .baseUrl("http://127.0.0.1:8080")
            .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .build()

    @Test
    fun `Get book by Title`() {
        val bookTitle = "Title5"
        repository.save(Book(title = bookTitle, author = "Author"))

        client.get().uri("/books/{title}", bookTitle)
                .exchange().expectStatus().isOk
                .expectBody()
                .jsonPath("$.title").isEqualTo(bookTitle)
    }

}
